
import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms'
import { ActivatedRoute, Router } from '@angular/router';
import { ProductsService } from '../services/products.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html'
})
export class ProductComponent implements OnInit {

 
  productForm: FormGroup;
  title: string = "Add";
  id: number = 0;
  prodCatId:number =0;
  contacts: any;
  errorMessage: any;
  submitted: boolean = false;
  prodcatList :Array<any>[];  
  _ref:any;
 
  constructor(private _fb: FormBuilder, 
              private _avRoute: ActivatedRoute,
              private _productService: ProductsService,
              private _router: Router) { 
    
    if(this._avRoute.snapshot.params["id"]){
      this.id = parseInt( this._avRoute.snapshot.params["id"]);
      console.log(this.id);
        this.title = 'Edit';
    }

    this.productForm = this._fb.group({
      productId: 0,
      prodCatId : 0,
      prodName: ['', [Validators.required, Validators.maxLength(30)]],
      prodDescription: ['', [Validators.required]]
    })
  }
  
  ngOnInit() {
    if(this.id > 0){     
        this._productService.getProductById(this.id)
          .subscribe(
          resp => 
                {             
                  this.productForm.setValue(resp)                   
                } 
                , error => this.errorMessage = error);
    }
    
      this._productService.getCategory()
        .subscribe(category => { 
          this.prodcatList = category;           
        }, error => this.errorMessage = <any>error);     
    
  }

  save(){
    debugger;
    if(!this.productForm.valid){
      this.submitted = true;
      return;
    }
    if(this.id > 0)
    {
      this._productService.updateProduct(this.id,this.productForm.value)
        .subscribe(proId => {
            //alert('Saved Successfully!')
            this._router.navigate(['products', {id: proId}]);
         }, error => this.errorMessage = error )
    }
    else{
    this._productService.saveProduct(this.productForm.value)
        .subscribe(proId => {
            //alert('Saved Successfully!')
            this._router.navigate(['products', {id: proId}]);
         }, error => this.errorMessage = error )
        }
  }

  cancel(){
    this._router.navigate(["products", {id: this.id}]);
  }

  get prodName() { return this.productForm.get('prodName'); }  
  get prodDescription() { return this.productForm.get('prodDescription'); }
  
  
}