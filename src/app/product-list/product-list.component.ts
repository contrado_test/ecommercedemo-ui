import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ProductsService } from '../services/products.service';


@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html'
})
export class ProductListComponent implements OnInit {

  productList: Array<any> = [];
  errorMessage: any;
  currentId: number = 0;
  pageIndex : number =1;
  pageSize : number = 2;
  pageSizeArray : Array<number> = [];  
 
  
  constructor( private _productService : ProductsService,
               private _router: Router,
               private _activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    if(this._activatedRoute.snapshot.params["id"])
      this.currentId = parseInt(this._activatedRoute.snapshot.params["id"]);
    this.getProducts(this.pageIndex,this.pageSize);

    this.pageSizeArray = [ 2, 5, 10,25,50,100,150,200 ];
  }

  getProducts(pageIndex,pageSize){
    this._productService.getProducts(pageIndex,pageSize).subscribe(
        data => this.productList = data.products,
        error => { 
          this.errorMessage = error
        }
    )
  }

  add(){
    this._router.navigate(['products/add']);
  }
  edit(id){
    this._router.navigate(['products/edit/' + id])
  }
  delete(id){
    var ans = confirm("Do you want to delete customer with Id: " + id);
    if(ans){
      this._productService.deleteProduct(id)
          .subscribe(  data=> {
            var index = this.productList.findIndex(x=>x.id == id);
            this.productList.splice(index, 1);
          }, error=> this.errorMessage = error )
    }
}

changed(e){
  console.log(e);
  this.pageSize=e.target.value;
  this.getProducts(this.pageIndex,this.pageSize);
}
}