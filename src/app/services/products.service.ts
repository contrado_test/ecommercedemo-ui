
import { Injectable } from '@angular/core';
import { Http, Response} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { forkJoin } from 'rxjs';
import { environment } from '../../environments/environment'

@Injectable({
  providedIn: 'root'
})

export class ProductsService {

  //baseUrl: string = 'http://localhost:58065/api/'
  baseUrl: string = environment.apiUrl

    constructor(private _http: Http) { }

    getProducts(pageIndex,pageSize): Observable<ProductDetailsPagatation>{
      return this._http.get(this.baseUrl +  pageIndex + '/' + pageSize +'/products')
                .map((response: Response) =>response.json())
                .catch(this._errorHandler);
    }

    getProductById(id) {
      return this._http.get(this.baseUrl +"product/"+ id)
              .map((response: Response) => {
               var d = response.json()
                let p: Product = {
                  productId: d.productId,
                  prodCatId :d.prodCatId,
                  prodName: d.prodName,
                  prodDescription:d.prodDescription     
                 
              }
                return p;
              })
              .catch(this._errorHandler)
    }
    
    getCategory():Observable<Array<any>> {
      return this._http.get(this.baseUrl +'category/all')
              .map((response: Response) => response.json())
              .catch(this._errorHandler)
    }

    getProductById_Category(id: number): Observable<ProductDetails> {
      return forkJoin([
        this._http.get(this.baseUrl +"product/"+ id).map(res => res.json()),
        this._http.get(this.baseUrl +'category/all').map(res => res.json())
      ])
      .map((data: any[]) => {
        let product: any = data[0];
        let categories: Array<any>[] = data[1];

        let x: ProductDetails = {
          productId: product.productId,
          prodCatId :product.prodCatId,
          prodName: product.prodName,
          prodDescription:product.prodDescription ,       
          prodcatList:categories
      }
        return x;
      });
    }

    updateProduct(id,product){
      return this._http.put(this.baseUrl +   'product/' + id, product)
              .map((response: Response) => response.json())
              .catch(this._errorHandler)
    }

    saveProduct(product){
      debugger;
      return this._http.post(this.baseUrl +   'product', product)
              .map((response: Response) => response.json())
              .catch(this._errorHandler)
    }

    deleteProduct(id){
      return this._http.delete(this.baseUrl + "product/" + id)
                .map((response:Response) =>  response.json())
                .catch(this._errorHandler)
    }

    _errorHandler(error:Response){debugger;
      console.log(error);
      
      return Observable.throw(error || "Internal server error");
    }
}

export interface ProductDetailsPagatation {
  pageSize: number;
  pageIndex: number;
  totalRecord: number;
  products:Array<any>;
}

export interface ProductDetails {
  productId: number,
  prodCatId :number,
  prodName: string,
  prodDescription:string
  prodcatList:Array<any>
}

export interface Product {
  productId: number,
  prodCatId :number,
  prodName: string,
  prodDescription:string
}

